data = """This is data, data is data.
Data can mean something, data can mean nothing.
Data can be simple, data can be complex.
Computations can be fast, computations can be slow."""

ngram_size = 3

ngrams = []
for i in range(len(data) - ngram_size):
    ngrams.append("")
    for j in range(ngram_size):
        ngrams[i] += data[i + j]

characters = []
for i in range(len(data)):
    if (data[i] not in characters):
        characters.append(data[i])

table = []
for i in range(len(ngrams)):
    table.append([])
    for j in range(len(characters)):
        table[i].append(0)

for i in range(len(ngrams)):
    table[
        ngrams.index(
            data[i : i + ngram_size]
        )
    ][
        characters.index(
            data[i + ngram_size]
        )
    ] += 1

sum = []
for i in range(len(ngrams)):
    sum.append(0)
    for j in range(len(characters)):
        sum[i] += table[i][j]


for i in range(len(ngrams)):
    for j in range(len(characters)):
        if (sum[i] > 0):
            table[i][j] /= sum[i]


from random import random

def weighted_random(w):
    r = random()
    for i in range(len(w)):
        if (w[i] > 0 and r < w[i]):
            return i
    return len(w) - 1

text = "Data"

def last_ngram():
    return text[len(text) - ngram_size : len(text)]

def getrow():
    if last_ngram() in ngrams:
        return table[ngrams.index(last_ngram())]
    else:
        for i in range(ngram_size):
            try:
                return table[
                    [
                        j[i:ngram_size] == last_ngram()[i:ngram_size]
                        for j in ngrams
                    ]
                    .index(True)
                ]
            except:
                pass
        return -1


for i in range(1000):
    if (c := getrow()) == -1:
        break
    text += characters[weighted_random(c)]

print(text)
